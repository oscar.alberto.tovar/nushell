# syntax=docker/dockerfile:1

FROM registry.gitlab.com/oscar.alberto.tovar/nushell:latest AS builder

ARG TARGETPLATFORM

COPY scripts/download-nu-from-github-releases.nu .

RUN <<EOF
set -eu

apt-get update
apt-get install -yqq --no-install-recommends ca-certificates

nu download-nu-from-github-releases.nu
EOF

FROM debian:stable-slim

COPY config.nu env.nu /root/.config/nushell/
COPY --from=builder /usr/bin/ /usr/bin/
COPY --from=builder /etc/ssl/certs/ /etc/ssl/certs/

ENTRYPOINT [ "/usr/bin/nu" ]
