variable "IMAGE_TAG" {
  default = "$IMAGE_TAG"
}

group "default" {
  targets = ["nushell"]
}

target "nushell" {
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    "linux/arm64"
  ]
  tags = [
    "registry.gitlab.com/oscar.alberto.tovar/nushell:${IMAGE_TAG}",
    "registry.gitlab.com/oscar.alberto.tovar/nushell:latest"
  ]
  attest = [
    "type=provenance,mode=min",
    "type=sbom"
  ]
  output = [
    "type=registry,compression=zstd,compression-level=22,force-compression=true"
  ]
}
