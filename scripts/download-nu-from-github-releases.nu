#!/usr/bin/env nu

let targetplatform = ($env | get -i TARGETPLATFORM | default (uname).machine)

let arch = match $targetplatform {
  "linux/amd64" | "x86_64" => "x86_64",
  "linux/arm64" | "aarch64" | "arm64" => "aarch64",
  _ => { print $"($targetplatform) not supported"; exit 1 },
}

let tag = (http get --headers [accept application/json] https://github.com/nushell/nushell/releases/latest | get tag_name)
let url = $"https://github.com/nushell/nushell/releases/download/($tag)/nu-($tag)-($arch)-unknown-linux-gnu.tar.gz"

http get --raw $url | tar --strip-components=1 -C /usr/bin -xzv
